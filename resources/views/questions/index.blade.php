@extends('layouts.hr')
@section('content')
    <div id="content-wrapper" style="margin-top: 65px">
        <div class="container-fluid">
            <h4>Questions</h4>
            <a class="btn btn-primary" href="{{route('questions.create')}}">+Add</a>
            <table class="table table-hover">
                <tr>
                    <th>id</th>
                    <th>text</th>
                    <th>edit</th>
                </tr>
                @foreach($questions as $question)
                    <tr>
                        <td>{{$question->id}}</td>
                        <td>{{$question->text}}</td>
                        <td>
                            <a href="{{route('questions.show', ['id' => $question->id])}}">edit</a>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection
