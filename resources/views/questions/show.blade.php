@extends('layouts.hr')
@section('content')
    <div id="content-wrapper" style="margin-top: 65px;">
        <div class="container-fluid">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br/>
            @endif
            <div class="row">
                <div class="col-md-8">
                    <form method="post" action="{{route('questions.update')}}">
                        @csrf
                        <div class="container">
                            <div class="row">
                                <input type="number" hidden value="{{$question->id}}" name="id">
                                <input type="number" hidden value="{{$question->position_id}}" name="position_id">
                                <div class="col-md-12">
                                    <label for="text">Name</label>
                                    <input type="text" class="form-control" id="name" name="name" required
                                           value="{{$question->name}}">
                                </div>
                                <div class="col-md-12">
                                    <label for="text">Text</label>
                                    <textarea class="form-control" class="w-100" name="text" id="text" cols="30" required
                                              rows="10">{{$question->text}}</textarea>
                                </div>
                                <div class="col-md-12">
                                    <button class="btn btn-primary">Done</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-4">
                    <div class="parent">
                        @foreach($question->buttons as $button)
                            <div class="child">
                                <label for="{{$button->id}}">{{$button->point}}</label><br>
                                <button style="margin-top: 5px" class="btn btn-warning"
                                        id="{{$button->id}}">{{$button->text}}</button>
                            </div>
                        @endforeach
                    </div>
                    <form method="post" action="{{route('buttons.store')}}">
                        @csrf
                        <div class="row" style="margin-top: 20px">
                            <input type="number" hidden value="{{$question->id}}" name="question_id">
                            <div class="col-6">
                                <label for="text">Name of button</label>
                                <input type="text" placeholder="Name of button" class="form-control" name="text">
                            </div>
                            <div class="col-6">
                                <label for="point">Point</label>
                                <input type="number" placeholder="Point" class="form-control" name="point">
                            </div>
                        </div>
                        <button class="btn btn-primary" style="margin-top: 10px">Add button</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
