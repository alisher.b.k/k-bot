@extends('layouts.hr')
@section('content')
    <div id="content-wrapper" style="margin-top: 65px;">
        <div class="container-fluid">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
            @endif
            <form method="post" action="{{route('questions.store')}}">
                @csrf
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <input type="number" hidden id="position_id" name="position_id" value="{{$positionId}}">
                            <div>
                                <label for="text">Name</label>
                                <input type="text" class="form-control" id="name" name="name" required>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div>
                                <label for="text">Text</label>
                                <textarea required class="w-100 form-control" name="text" id="text" cols="30" rows="10" maxlength ="1500"></textarea>
                            </div>
                        </div>
                        <div class="mt-3">
                            <button class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
