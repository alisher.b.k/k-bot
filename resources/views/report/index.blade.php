@extends('layouts.hr')
@section('content')
    <div id="content-wrapper" style="margin-top: 65px">
        <div class="container-fluid">
            <h4>Questions</h4>
            <table class="table table-hover">
                <tr>
                    <th>name</th>
                    <th>candidates count</th>
                </tr>
                @foreach($positions as $position)
                    <tr>
                        <td>
                            {{$position->name}}
                        </td>
                        <td>
                            {{$position->candidates_count}}
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection
