@extends('layouts.hr')
@section('content')
    <div id="content-wrapper" style="margin-top: 65px;">
        <div class="container-fluid">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
            @endif
            <form action="{{route('company.edit', ['company' => $company])}}">
                @method('PUT')
                @csrf
                <div>
                    <label for="text">Name</label>
                    <input class="form-control" name="name" id="name" value="{{$company->name}}"/>
                </div>
                <div>
                    <label for="description">Description</label>
                    <textarea class="w-100 form-control" name="description" id="description" cols="30" rows="10">{{$company->description}}</textarea>
                </div>
                <button class="btn btn-primary">Update</button>
            </form>
        </div>
    </div>
@endsection
