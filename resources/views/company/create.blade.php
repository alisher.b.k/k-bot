@extends('layouts.hr')
@section('content')
    <div id="content-wrapper" style="margin-top: 65px;">
        <div class="container-fluid">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
            @endif
            <form method="post" action="{{route('company.store')}}">
                @csrf
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="text">Name</label>
                            <input class="form-control"
                                   name="name"
                                   id="name"
                                   required/>
                        </div>
                        <div class="col-md-12">
                            <label for="description">Description</label>
                            <textarea class="w-100 form-control"
                                      name="description"
                                      required
                                      id="description"
                                      cols="30" rows="10"
                                      maxlength="1500"></textarea>

                            <button class="btn btn-primary mt-1">Save</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
