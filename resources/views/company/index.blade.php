@extends('layouts.hr')
@section('content')
    <div id="content-wrapper" style="margin-top: 65px">
        <div class="container-fluid">
            <h4>Companies</h4>
            <a class="btn btn-primary" href="{{route('company.create')}}">+ Create Company</a>
            <table class="table table-hover">
                <tr>
                    <th>id</th>
                    <th>name</th>
                </tr>
                @foreach($companies as $company)
                    <tr>
                        <td>{{$company->id}}</td>
                        <td>{{$company->name}}</td>
                        <td>
                            <div style="display: flex">
                                <a class="btn btn-primary" href="{{route('company.show', ['company' => $company])}}">edit</a>
                                <form action="{{route('company.destroy', ['company' => $company])}}" method="post">
                                    @method('DELETE')
                                    @csrf
                                    <button class="btn btn-rounded btn-close" type="submit" value="Delete" data-toggle="tooltip" title="Delete!"></button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </table>
            {{$companies->links()}}
        </div>
    </div>
@endsection
