@extends('layouts.hr')
@section('content')
    <div id="content-wrapper" style="margin-top: 65px">
        <div class="container-fluid">
            <h4>Candidates</h4>
            <a class="btn btn-primary" href="{{route('candidates.create')}}">+Add</a>
            <table class="table table-hover">
                <tr>
                    <th>id</th>
                    <th>name</th>
                    <th>family_name</th>
                    <th>phone</th>
                    <th>email</th>
                    <th>city</th>
                    <th>edit</th>
                </tr>
                @foreach($candidates as $candidate)
                    <tr>
                        <td>{{$candidate->id}}</td>
                        <td>{{$candidate->name}}</td>
                        <td>{{$candidate->family_name}}</td>
                        <td>{{$candidate->phone}}</td>
                        <td>{{$candidate->email}}</td>
                        <td>{{$candidate->city}}</td>
                        <td>
                            <a href="{{route('candidates.show', ['id' => $candidate->id])}}">edit</a>
                        </td>
                    </tr>
                @endforeach
            </table>
            {{$candidates->links()}}
        </div>
    </div>
@endsection
