@extends('layouts.hr')
@section('content')
    <div id="content-wrapper" style="margin-top: 65px;">
        <div class="container-fluid">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
            @endif
            <form method="post" action="{{route('candidates.store')}}">
                @csrf
                <label for="name">Name</label>
                <input type="text" name="name" class="form-control" id="name">
                <label for="family_name">Family Name</label>
                <input type="text" name="family_name" class="form-control" id="family_name">
                <label for="phone">Phone</label>
                <input type="text" name="phone" class="form-control" id="phone">
                <label for="email">Email</label>
                <input type="text" name="email" class="form-control" id="email">
                <label for="city">City</label>
                <input type="text" name="city" class="form-control" id="city">
                <button class="btn btn-primary">Save</button>
            </form>
        </div>
    </div>
@endsection
