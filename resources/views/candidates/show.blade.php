@extends('layouts.hr')
@section('content')
    <div id="content-wrapper" style="margin-top: 65px;">
        <div class="container-fluid">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
            @endif
            <form method="post" action="{{route('candidates.store')}}">
                @csrf
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="name">Name</label>
                            <input type="text" name="name" class="form-control" id="name" value="{{$candidate->name}}">
                        </div>
                        <div class="col-md-6">
                            <label for="family_name">Family Name</label>
                            <input type="text" name="family_name" class="form-control" id="family_name" value="{{$candidate->family_name}}">
                        </div>
                        <div class="col-md-6">
                            <label for="phone">Phone</label>
                            <input type="text" name="phone" class="form-control" id="phone" value="{{$candidate->phone}}">
                        </div>
                        <div class="col-md-6">
                            <label for="email">Email</label>
                            <input type="text" name="email" class="form-control" id="email" value="{{$candidate->email}}">
                        </div>
                        <div class="col-md-6">
                            <label for="city">City</label>
                            <input type="text" name="city" class="form-control" id="city" value="{{$candidate->city}}">
                            <button class="btn btn-primary mt-3">Save</button>
                        </div>
                    </div>
                </div>
            </form>
            <div class="container mt-3">
                <span>Ответы кандидата</span>

                <table class="table table-hover">
                    <tr>
                        <th>id</th>
                        <th>name</th>
                        <th>salary</th>
                        <th>point</th>
                    </tr>
                    @foreach($positions as $position)
                        <tr>
                            <td>{{$position->id}}</td>
                            <td>{{$position->name}}</td>
                            <td>{{$position->salary}}</td>
                            <td>{{$position->questions->sum('answer_sum_point')}}</td>
                        </tr>
                    @endforeach
                </table>
                Sum of points: <b>{{$candidate->answers->sum('point')}}</b>
            </div>
        </div>
    </div>
@endsection
