@extends('layouts.hr')
@section('content')
    <div id="content-wrapper" style="margin-top: 65px">
        <div class="container-fluid">
            <h4>Positions</h4>
            <a class="btn btn-primary" href="{{route('position.create')}}">+ Create Position</a>
            <table class="table table-hover">
                <tr>
                    <th>id</th>
                    <th>name</th>
                    <th>Company</th>
                </tr>
                @foreach($positions as $position)
                    <tr>
                        <td>{{$position->id}}</td>
                        <td>{{$position->name}}</td>
                        <td>{{$position->company->name}}</td>
                        <td>
                            <div style="display: flex">
                                <a class="btn btn-primary" href="{{route('position.show', ['position' => $position])}}">edit</a>
                                <form action="{{route('position.destroy', ['position' => $position])}}" method="post">
                                    @method('DELETE')
                                    @csrf
                                    <button class="btn btn-rounded btn-close" type="submit" value="Delete" data-toggle="tooltip" title="Delete!"></button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </table>
            {{$positions->links()}}
        </div>
    </div>
@endsection
