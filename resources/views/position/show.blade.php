@extends('layouts.hr')
@section('content')
    <div id="content-wrapper" style="margin-top: 65px;">
        <div class="container-fluid">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br/>
            @endif
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <form action="{{route('position.edit', ['position' => $position])}}">
                            @method('PUT')
                            @csrf
                            <div class="row">
                                <div class="col-md-6">
                                    <div>
                                        <label for="text">Name</label>
                                        <input class="form-control" name="name" id="name" value="{{$position->name}}"/>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="company_id">Company</label>
                                    <select class="form-select" name="company_id" id="company_id">
                                        <option value="0" disabled selected>Select company</option>
                                        @foreach($companies as $company)
                                            <option value="{{$company->id}}"
                                                    @if($position->company->id === $company->id) selected @endif>{{$company->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="salary">Salary</label>
                                        <input class="form-control" type="text" name="salary" id="salary" value="{{$position->salary}}">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="work_schedule">Schedule</label>
                                        <input type="text" class="form-control" name="work_schedule" id="work_schedule" value="{{$position->work_schedule}}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div>
                                        <label for="description">Description</label>
                                        <textarea class="w-100 form-control" name="description" id="description"
                                                  cols="30" rows="10">{{$position->description}}</textarea>
                                    </div>
                                </div>
                                <button class="btn btn-primary mt-2">Update</button>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6">
                        <div class="row mt-4">
                            <div class="col-md-12">
                                @foreach($position->questions as $question)
                                    @php $loopId = $loopId + 1 @endphp
                                    <div  class="card mb-1" style="display: flex; flex-direction: row; !important; justify-content: space-between !important; padding: 10px 20px">
                                        <a href="{{route('questions.show', ['id' => $question->id])}}" style="cursor: pointer; text-decoration: none; color: black">
                                            <span><b>{{$loopId}})</b> {{$question->text}}</span>
                                        </a>
                                        <a class="btn btn-close" href="{{route('questions.destroy', ['id' => $question->id])}}"></a>
                                    </div>
                                @endforeach
                            </div>
                            <div style="display: flex; justify-content: end">
                                <a href="{{route('questions.create', ['positionId' => $position->id])}}" type="submit" class="btn btn-primary" id="button-add">Add Question</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
