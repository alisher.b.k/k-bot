@extends('layouts.hr')
@section('content')
    <div id="content-wrapper" style="margin-top: 65px;">
        <div class="container-fluid">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
            @endif
            <form method="post" action="{{route('position.store')}}">
                @csrf
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="text">Name</label>
                            <input class="form-control" name="name" id="name" required/>
                        </div>
                        <div class="col-md-6">
                            <label for="company_id">Company</label>
                            <select required class="form-select" name="company_id" id="company_id">
                                <option value="0" disabled selected>Select company</option>
                                @foreach($companies as $company)
                                    <option value="{{$company->id}}">{{$company->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label for="salary">Salary</label>
                            <input class="form-control" type="text" name="salary" id="salary">
                        </div>
                        <div class="col-md-6">
                            <label for="work_schedule">Schedule</label>
                            <input type="text" class="form-control" name="work_schedule" id="work_schedule">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label for="description">Description</label>
                            <textarea required class="w-100 form-control" name="description" id="description" cols="30" rows="10"></textarea>
                        </div>
                    </div>
                    <button class="btn btn-primary mt-2">Save</button>
                </div>
            </form>
        </div>
    </div>
@endsection
