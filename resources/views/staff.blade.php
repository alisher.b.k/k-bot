@extends('layouts.app')
@section('content')
<div id="content-wrapper">
    <div class="container-fluid">
        <!-- DataTables Example -->
            <div class="card mb-3">
                <div class="card-header">
                    <i class="fas fa-table"></i>
                    Сотрудники
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table table-striped table-hover" id="datatable" width="100%" cellspacing="0">
                        <thead class='table-dark'>
                            <tr>
                                <th>ФИО</th>
                                <th>Должность</th>
                                <th>Рабочий телефон</th>
                                <th>Мобильный телефон</th>
                                <th>Email</th>
                                <th>Опыт</th>
                                <th>Компания</th>
                            </tr>
                        </thead>
                        <tfoot class='table-dark' >
                            <tr>
                                <th>ФИО</th>
                                <th>Должность</th>
                                <th>Рабочий телефон</th>
                                <th>Мобильный телефон</th>
                                <th>Email</th>
                                <th>Опыт</th>
                                <th>Компания</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            @foreach($staff as $person)
                            <tr>
                                <td>{{$person->FIO}}</td>
                                <td>{{$person->position}}</td>
                                <td>{{$person->workPhone}}
                                    @if($person->intPhone)
                                        (вн. {{$person->intPhone}})
                                    @endif
                                </td>
                                <td>{{$person->mobPhone}}</td>
                                <td>{{$person->email}}</td>
                                <td>{{$person->workEx}}</td>
                                <td>{{$person->WORKPLACE}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                        </table>
                    </div>
                </div>
            </div>
    </div>
</div>

<script
  src="https://code.jquery.com/jquery-3.6.0.min.js"
  integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
  crossorigin="anonymous"></script>

<script>
    $(document).ready(function() {
        $('#datatable').DataTable();
    });
</script>
@endsection