<?php

use App\Http\Controllers\AnswerController;
use App\Http\Controllers\API\CommandController;
use App\Http\Controllers\ButtonsController;
use App\Http\Controllers\CandidatesController;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\PositionController;
use App\Http\Controllers\QuestionsController;
use App\Http\Controllers\ReportController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\BookingController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ExportController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('index');
Route::post('/position/add-question/{position}', [PositionController::class, 'addQuestion'])->name('position.questions');
Route::post('/sendNotifications', [HomeController::class, 'sendNotifications'])->name('sendNotifications');
Route::get('/export/reactions', [ExportController::class,'getReactionsView'])->name('export.reactions.view');
Route::post('/export/get_reactions', [ExportController::class,'getReactionsForExport'])->name('export.get.reactions');
Route::post('/export/reactions', [ExportController::class, 'exportReaction'])->name('export.reaction');
Route::get('/questions', [QuestionsController::class, 'index'])->name('questions.index');
Route::get('/questions/create/{positionId}', [QuestionsController::class, 'create'])->name('questions.create');
Route::post('/questions/store', [QuestionsController::class, 'store'])->name('questions.store');
Route::get('/questions/show/{id}', [QuestionsController::class, 'show'])->name('questions.show');
Route::post('/questions/update', [QuestionsController::class, 'update'])->name('questions.update');
Route::get('/questions/destroy/{id}', [QuestionsController::class, 'destroy'])->name('questions.destroy');

Route::post('/buttons/store', [ButtonsController::class, 'store'])->name('buttons.store');

Route::get('/candidates', [CandidatesController::class, 'index'])->name('candidates.index');
Route::get('/candidates/create', [CandidatesController::class, 'create'])->name('candidates.create');
Route::post('/candidates/store', [CandidatesController::class, 'store'])->name('candidates.store');
Route::get('/candidates/show/{id}', [CandidatesController::class, 'show'])->name('candidates.show');
Route::post('/candidates/delete/{id}', [CandidatesController::class, 'delete'])->name('candidates.delete');
Route::get('botan/staff/get', [HomeController::class, 'showStaff'])->name('showStaff');

Route::get('/report', [ReportController::class, 'index'])->name('report');
Route::resource('company', CompanyController::class);
Route::resource('position', PositionController::class);
Route::get('/admin/command', [CommandController::class, 'command']);
