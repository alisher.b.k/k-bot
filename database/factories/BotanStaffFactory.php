<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class BotanStaffFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'FIO' => $this->faker->firstName.' '.$this->faker->lastName,
            'WORKPLACE' => $this->faker->company,
            'position' => $this->faker->jobTitle,
            'workPhone' => $this->faker->phoneNumber,
            'intPhone' => $this->faker->phoneNumber,
            'mobPhone' => $this->faker->phoneNumber,
            'email' => $this->faker->unique()->email,
            'telegramId' => random_int(100000000, 999999999),
            'bday' => $this->faker->dateTimeBetween('-70 years', '-18 years'),
            'workEx' => $this->faker->dateTimeBetween('-10 years', now()),
        ];
    }
}
