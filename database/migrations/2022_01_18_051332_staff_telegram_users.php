<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class StaffTelegramUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stafftelegramusers', function (Blueprint $table) {
            $table->id();
            $table->string('staffEmail')->nullable();
            $table->string('telegramId')->nullable();
            $table->string('telegramUsername')->nullable();
            $table->string('telegramFirstName')->nullable();
            $table->string('telegramLastName')->nullable();
            $table->string('updatesSubscription')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stafftelegramusers');
    }
}
