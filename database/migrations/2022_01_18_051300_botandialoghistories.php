<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Botandialoghistories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('botan_dialog_histories', function (Blueprint $table) {
            $table->id();
            $table->string('chatId')->nullable();
            $table->string('telegramUsername', 100)->nullable();
            $table->string('telegramFirstName', 300)->nullable();
            $table->string('telegramLastName', 100)->nullable();
            $table->text('request')->nullable();
            $table->text('response')->nullable();
            $table->string('userEmail')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('botan_dialog_histories');
    }
}
