<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Botanstaff extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('botan_staff', function (Blueprint $table) {
            $table->id();
            $table->string('FIO')->nullable();
            $table->string('position')->nullable();
            $table->string('photo')->nullable();
            $table->string('workPhone')->nullable();
            $table->string('intPhone')->nullable();
            $table->string('mobPhone')->nullable();
            $table->string('email')->nullable();
            $table->string('bday')->nullable();
            $table->string('workEx')->nullable();
            $table->string('ISN')->nullable();
            $table->string("telegramId")->nullable();
            $table->string('NAMELAT')->nullable();
            $table->string('WORKPLACE')->nullable();
            $table->string('MAINDEPT')->nullable();
            $table->string('SUBDEPT')->nullable();
            $table->string('VACATIONBEG')->nullable();
            $table->string('VACATIONEND')->nullable();
            $table->string('TRIPBEG')->nullable();
            $table->string('TRIPEND')->nullable();
            $table->string('LEAVEDATE')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('botan_staff');
    }
}
