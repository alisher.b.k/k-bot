<?php

namespace App\Http\Controllers;

use App\Models\Button;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ButtonsController extends Controller
{
    public function store(Request $request)
    {
        try {
            $input = $request->only(['question_id', 'text', 'point']);
            Button::create($input);
            return redirect()->back(301);
        } catch (\Exception $e) {
            Log::debug($e->getMessage());
            return view('questions.create')->with(['error' => $e->getMessage()]);
        }
    }
}
