<?php

namespace App\Http\Controllers;

use App\Http\Requests\CompanyRequest;
use App\Models\Company;

class CompanyController extends Controller
{
    public function index()
    {
        $companies = Company::query()->paginate(15);
        return view('company.index', compact('companies'));
    }

    public function create()
    {
        return view('company.create');
    }

    public function store(CompanyRequest $request)
    {
        Company::query()->create($request->validated());
        return redirect(route('company.index'));
    }

    public function show(Company $company)
    {
        return view('company.show', compact('company'));
    }

    public function edit(CompanyRequest $request, Company $company)
    {
        $company->updateOrFail($request->validated());
        return redirect(route('company.index'));
    }

    public function destroy(Company $company)
    {
        $company->delete();
        return redirect(route('company.index'));
    }
}
