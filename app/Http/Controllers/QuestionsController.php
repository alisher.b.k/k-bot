<?php

namespace App\Http\Controllers;

use App\Models\Button;
use App\Models\Candidate;
use App\Models\Position;
use App\Models\Question;
use Illuminate\Http\Request;

class QuestionsController extends Controller
{
    public function index()
    {
        $questions = Question::get();
        return view('questions.index', compact('questions'));
    }

    public function create($positionId)
    {
        return view('questions.create', compact('positionId'));
    }

    public function store(Request $request)
    {
        try {
            $question = Question::create($request->except('_token'));
            return redirect(route('questions.show', ['id' => $question->id]));
        } catch (\Exception $e) {
            return view('questions.create', ['positionId' => $request->position_id])->with(['error' => $e->getMessage()]);
        }
    }

    public function show($id)
    {
        $question = Question::with('buttons')->find($id);
        return view('questions.show', compact('question'));
    }

    public function update(Request $request)
    {
        $input = $request->only(['id', 'text', 'name']);
        Question::updateOrCreate(['id' => $input['id']],$input);
        $position = Position::find($request->position_id);
        return redirect(route('position.show', ['position'=>$position]));
    }

    public function destroy($id)
    {
        Button::where('question_id', $id)->delete();
        Question::where('id', $id)->delete();
        return redirect()->back();
    }

    public function getReport(Request $request)
    {
        $candidates = Candidate::with('answers')->paginate();
        return view('candidates.report', compact('candidates'));
    }
}
