<?php

namespace App\Http\Controllers;

use App\Http\Requests\PositionRequest;
use App\Models\Company;
use App\Models\Position;
use App\Models\Question;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class PositionController extends Controller
{
    public function index()
    {
        $positions = Position::query()->paginate(15);
        return view('position.index', compact('positions'));
    }

    public function create()
    {
        $companies = Company::query()->get();
        return view('position.create', compact('companies'));
    }

    public function store(PositionRequest $request)
    {
            Position::query()->updateOrCreate($request->validated());
        return redirect(route('position.index'));
    }

    public function show(Position $position)
    {
        $companies = Company::query()->get();
        $questions = Question::query()->get();
        $loopId = 0;
        return view('position.show', compact('position', 'companies', 'questions', 'loopId'));
    }

    public function edit(PositionRequest $request, Position $position)
    {
        $position->updateOrFail($request->validated());
        return redirect(route('position.index'));
    }

    public function destroy(Position $position)
    {
        $position->delete();
        return redirect(route('position.index'));
    }

    public function addQuestion(Position $position, Request $request)
    {
        $questionId = $request->question_id;
//        PositionQuestions::query()->updateOrCreate([
//            'position_id' => $position->id,
//            'question_id' => $questionId
//        ]);
        return redirect(route('position.show', ['position' => $position]));
    }
}
