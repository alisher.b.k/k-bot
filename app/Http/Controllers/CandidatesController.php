<?php

namespace App\Http\Controllers;

use App\Models\Answer;
use App\Models\Candidate;
use App\Models\Position;
use App\Models\Question;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class CandidatesController extends Controller
{
    public function index()
    {
        $candidates = Candidate::paginate(25);
        return view('candidates.index', compact('candidates'));
    }

    public function create()
    {
        return view('candidates.create');
    }

    public function store(Request $request)
    {
        try {
            Candidate::create($request->except(['_token']));
            return redirect(route('candidates.index'));
        } catch (\Exception $e) {
            return view('candidates.create')->withErrors(['error' => $e->getMessage()]);
        }
    }

    public function show($id)
    {
        $candidate = Candidate::with('answers')->find($id);
        $positions = Position::query()->whereHas('questions.answer')
            ->with(['questions' => function($query) {
                $query->withSum('answer', 'point');
            }] )->get();
        return view('candidates.show', compact('candidate', 'positions'));
    }

    public function update(Request $request)
    {
        $input = $request->only(['id', 'text']);
        Question::updateOrCreate(['id' => $input['id']],$input);
        return redirect(route('questions.index'));
    }

    public function getReport(Request $request)
    {
        $candidates = Candidate::with('answers')->paginate();
        return view('candidates.report', compact('candidates'));
    }

}
