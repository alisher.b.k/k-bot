<?php

namespace App\Http\Controllers;

use App\Models\Candidate;
use Carbon\Carbon;

class AnswerController extends Controller
{
    public function index(){
        $startDate = Carbon::parse(now())->subMonth()->toDateTimeString();
        $endDate = Carbon::parse(now())->toDateTimeString();

        $candidates = Candidate::query()
            ->whereBetween('updated_at', [$startDate, $endDate])
            ->get();
        return view('report.index', compact('candidates'));
    }
}
