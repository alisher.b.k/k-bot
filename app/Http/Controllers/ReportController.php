<?php
namespace App\Http\Controllers;

use App\Models\Position;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    public function index()
    {
        $now = now();
        $positions = DB::select("
            select p.name, a.total_count as 'candidates_count'
            from positions p
            left join questions q on p.id = q.position_id
                join (
                    select a.question_id, count(a.candidate_id) as 'total_count'
                    from answers a
                    group by a.question_id
                    ) a on a.question_id = q.id
            group by p.name, a.total_count
        ");
        return view('report.index', compact('positions'));
    }

    public function getCandidateReport()
    {

    }

    public function getPositionReport()
    {

    }

    public function getCompanyReport()
    {

    }
}
