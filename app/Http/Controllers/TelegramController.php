<?php

namespace App\Http\Controllers;

use App\Models\Answer;
use App\Models\Candidate;
use App\Models\Company;
use App\Models\Position;
use App\Models\Question;
use Illuminate\Http\Request;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Laravel\Facades\Telegram;

class TelegramController extends Controller
{
    const CHOOSE_COMPANY = 'cc';
    const CHOOSE_POSITION = 'cp';
    const CHOOSED_ANSWER = 'cd';

    public function start(Request $request)
    {
        $replyMarkup = Keyboard::make([
            'resize_keyboard' => true,
            'one_time_keyboard' => true,
            'keyboard' => [
                [
                    ['text' => 'Начать'],
                ]
            ]]);
        Telegram::setAccessToken(env('HR_BOT_TOKEN'));
        Telegram::sendMessage([
            'chat_id' => $request->telegram_id,
            'text' => 'Welcome Message',
            'reply_markup' => $replyMarkup
        ]);
    }

//    public function getCompany(Request $request)
//    {
//        $companies = Company::get();
//        Telegram::setAccessToken(env('HR_BOT_TOKEN'));
//        Telegram::sendMessage([
//            'chat_id' => $request->telegram_id,
//            'text' =>"Выберите компанию",
//        ]);
//        foreach ($companies as $company) {
//            $replyMarkup = Keyboard::make([
//                'resize_keyboard' => true,
//                'one_time_keyboard' => true,
//                'inline_keyboard' => [
//                    [
//                        ['text' => 'Выбрать', 'callback_data' => $this->generateCallback($company->id, self::CHOOSE_COMPANY)],
//                    ]
//                ]]);
//            Telegram::sendMessage([
//                'chat_id' => $request->telegram_id,
//                'text' =>$company->description,
//                'reply_markup' => $replyMarkup
//            ]);
//        }
//        return true;
//    }

    public function callbackAction(Request $request)
    {
        [$tag, $id] = $array = explode(':', $request->data);
        if ($tag === self::CHOOSE_COMPANY) {
            Telegram::setAccessToken(env('HR_BOT_TOKEN'));
            Telegram::sendMessage([
                'chat_id' => $request->telegram_id,
                'text' =>"Выберите должность",
            ]);
            $this->getPositions($id, $request->telegram_id);
        } else if ($tag === self::CHOOSE_POSITION) {
            $this->getQuestion($id, $request->telegram_id);
        } else if ($tag === self::CHOOSED_ANSWER) {
            $candidate = Candidate::query()->where('telegram_id', $request->telegram_id)->first();
            $answer = Answer::query()->create([
                'candidate_id' => $candidate->id,
                'point' => $id,
                'question_id' => $array[2]
            ]);
            Telegram::setAccessToken(env('HR_BOT_TOKEN'));
            Telegram::editMessageText([
                'chat_id' => $request->telegram_id,
                'text' =>'Answered',
                'message_id' => $request->message_id,
            ]);
            $this->getQuestion($array[3], $request->telegram_id);
        }
    }

    private function generateCallback($id, $tag): string
    {
        return "$tag:$id";
    }

    public function getPositions($id, $telegramId)
    {
        $positions = Position::where('company_id', $id)->get();
        foreach ($positions as $position){
            $replyMarkup = Keyboard::make([
                'resize_keyboard' => true,
                'one_time_keyboard' => true,
                'inline_keyboard' => [
                    [
                        ['text' => 'Выбрать', 'callback_data' => $this->generateCallback($position->id, self::CHOOSE_POSITION).":$id"],
                    ]
                ]]);
            Telegram::setAccessToken(env('HR_BOT_TOKEN'));
            Telegram::sendMessage([
                'chat_id' => $telegramId,
                'text' =>$position->name,
                'reply_markup' => $replyMarkup
            ]);
        }
    }

//    public function getQuestion($positionId, $telegramId)
//    {
//        $candidate = Candidate::where('telegram_id', $telegramId)->first();
//        $question = Question::with('buttons')
//            ->whereDoesntHave('answer', function ($query) use($candidate) {
//                $query->where('candidate_id', $candidate->id);
//            })
//            ->with('answer')
//            ->where('position_id', $positionId)
//            ->first();
//        $buttons = [];
//        if (!$question) {
//            Telegram::setAccessToken(env('HR_BOT_TOKEN'));
//            Telegram::sendMessage([
//                'chat_id' => $telegramId,
//                'text' =>'Done',
//            ]);
//            return;
//        }
//        foreach ($question->buttons as $button) {
//            $buttons[] = ['text' => $button->text, 'callback_data' => $this->generateCallback($button->point, self::CHOOSED_ANSWER).":$question->id:$positionId"];
//        }
//        $replyMarkup = Keyboard::make([
//            'resize_keyboard' => true,
//            'one_time_keyboard' => true,
//            'inline_keyboard' => [
//                $buttons
//            ]]);
//        Telegram::setAccessToken(env('HR_BOT_TOKEN'));
//        Telegram::sendMessage([
//            'chat_id' => $telegramId,
//            'text' =>$question->text,
//            'reply_markup' => $replyMarkup
//        ]);
//    }
}
