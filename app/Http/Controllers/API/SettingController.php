<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\News;
use App\Models\NewsReactions;

class SettingController extends Controller
{
    
  public function saveReaction(Request $request){
        $pieces = explode("#poll", $request['data']);
        $newsList = News::where('id', $pieces[1])->get();
        foreach ($newsList as $news) {
                $existingReaction = NewsReactions::where('telegramId', $request['chat_id'])
                    ->where('news_id', $news->id)
                    ->where('reaction', $pieces[0])
                    ->first();
            if (!$existingReaction) {
                $news_reactions = new NewsReactions();
                $news_reactions->reaction = $pieces[0];
                $news_reactions->telegramId = $request['chat_id'];
                $news_reactions->news_id = $news->id;
                $news_reactions->save();

                return json_encode([
                    'result' => 'Спасибо за вашу оценку'
                ]);
            }
        }
    }
}
