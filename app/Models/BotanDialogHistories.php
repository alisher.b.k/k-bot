<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BotanDialogHistories extends Model
{
    protected $table = "botan_dialog_histories";

    protected $fillable = [
        'chatId', 'telegramUsername', 'telegramFirstName',
        'telegramLastName', 'request', 'response', 'userEmail',
    ];

    public function user()
    {
        return $this->belongsTo(BotanStaff::class, 'userEmail', 'email');
    }
}
