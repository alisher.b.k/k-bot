<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Answer extends Model
{
    use HasFactory;
    protected $fillable = ['question_id', 'candidate_id', 'point'];

    public function candidate(): HasMany
    {
        return $this->hasMany(Candidate::class, 'id', 'candidate_id');
    }
}
