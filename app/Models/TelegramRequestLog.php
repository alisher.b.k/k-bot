<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TelegramRequestLog extends Model
{
    // protected $table = "stafftelegramusers";
    // use HasFactory;
    protected $fillable = [
        'telegramId', 'command', 'json_data',
      ];
}
