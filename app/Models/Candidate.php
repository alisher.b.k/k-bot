<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;

class Candidate extends Model
{
    use HasFactory;
    protected $fillable = ['telegram_id', 'command', 'city', 'name', 'family_name', 'phone', 'email'];

    public function answers(): HasManyThrough
    {
        return $this->hasManyThrough(
            Question::class,
            Answer::class,
            'candidate_id',
            'id',
            'id',
            'question_id'
        )->select(['answers.id', 'questions.text', 'answers.point', 'questions.position_id']);
    }

}
