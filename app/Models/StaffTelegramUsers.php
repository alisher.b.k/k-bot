<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StaffTelegramUsers extends Model
{
    protected $table = "stafftelegramusers";
    use HasFactory;
}
