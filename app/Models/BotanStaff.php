<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BotanStaff extends Model
{
    use HasFactory;
    protected $table = "botan_staff";
}
