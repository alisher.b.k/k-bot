<?php
namespace App\Repository\Telegram\Utilities;

class Message
{
    public $from;
    public $chat;
    public $user;
    public $forward_from;
    public $forward_from_chat;
    public $message;

    public function __construct(Chat $chat, TelegramUser $tUser)
    {
        $this->chat = $chat;
        $this->user = $tUser;
        $this->forward_from = $tUser;
        $this->forward_from_chat = $chat;
        $this->message = "/start";
    }

    public function getText() {
        return $this->message;
    }
}
