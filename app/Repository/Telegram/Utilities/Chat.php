<?php

namespace App\Repository\Telegram\Utilities;

use Faker\Factory;

class Chat
{
    protected $faker;

    public $id;
    public $username;
    public $firstname;
    public $lastname;
    public $title;
    public $type = "message";

    public function __construct(TelegramUser $user)
    {
        $this->faker = Factory::create();
        $this->id = $this->faker->randomDigitNotNull;
        $this->username = $user->username;
        $this->firstname = $user->firstName;
        $this->lastname = $user->lastName;
        $this->title = $this->faker->title;
    }

    public function get(): array
    {
        return [
            'id' => $this->id,
            'type' => 'message',
            'title' => $this->title,
            'username' => $this->username,
            'firstname' => $this->firstname,
            'lastname' => $this->lastname,
            'photo' => null,
            'description' => null,
            'inviteLink' => null,
            'pinnedMessage' => null,
            'permissions' => null,
            'slowModeDelay' => null,
            'stickerSetName' => null,
            'canSetStickerSet' => null
        ];
    }
}
