<?php

namespace App\Repository\Telegram\Utilities;

use Faker\Factory;

class TelegramUser
{
    protected $faker;
    public $id;
    public $isBot;
    public $firstName;
    public $lastName;
    public $username;
    public $languageCode = "ru";

    public function __construct()
    {
        $this->faker = Factory::create();
        $this->id = $this->faker->randomDigitNotNull;
        $this->isBot = $this->faker->boolean;
        $this->username = $this->faker->userName;
        $this->lastName = $this->faker->lastName;
        $this->firstName = $this->faker->firstName;
    }
}
