<?php
namespace App\Repository\Telegram;

use App\Repository\Telegram\Utilities\Chat;
use App\Repository\Telegram\Utilities\Message;
use App\Repository\Telegram\Utilities\TelegramUser;

class TelegramMock implements TelegramInterface
{
    /**
     * @var TelegramMock
     */
    private static $instance;
    public static function instance(): TelegramMock
    {
        if (empty(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function initParams($api): array
    {
        $user = new TelegramUser();
        $chat = new Chat($user);
        $message = new Message($chat, $user);
        return [
            'chat' => $chat->get(),
            'chatId' =>  $chat->id,
            'username' => $chat->username,
            'firstname' => $chat->firstname,
            'lastname' => $chat->lastname,
            'message' => $message,
            'messageText' => strtolower($message->getText()),
        ];
    }
}
