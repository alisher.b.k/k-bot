<?php

namespace App\Repository\Telegram;

interface TelegramInterface
{
    public function initParams($api);
}
