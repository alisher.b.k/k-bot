<?php
namespace App\Repository\Telegram;

class Telegram implements TelegramInterface
{
    /**
     * @var TelegramMock
     */
    private static $instance;
    public static function instance(): TelegramMock
    {
        if (empty(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function initParams($api): array
    {
        $message = $api->getTelegram()->getWebhookUpdate()->getMessage();
        $chat = $message->getChat();
        return [
            'chat' => $chat,
            'chatId' =>  $chat->getId(),
            'username' => $chat->getUsername(),
            'firstname' => $chat->first_name,
            'lastname' => $chat->last_name,
            'message' => $message,
            'messageText' => strtolower($message->getText()),
        ];
    }
}
