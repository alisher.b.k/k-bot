<?php

namespace App\Telegram;

use App\Models\BotanStaff;
use App\Models\StaffTelegramUsers;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Laravel\Facades\Telegram;
use Telegram\Bot\Keyboard\Keyboard;

class CheckUser
{
    public static function index($chatId): array
    {
        try {
            $res = '';
            $telegramUser = StaffTelegramUsers::where('telegramId', $chatId)
                ->first();
            $user = null;
            if ($telegramUser) {
                $userEmail = strtolower($telegramUser->staffEmail);

                $user = BotanStaff::where("email", $userEmail)->first();
                if (!$user) {
                    $user = BotanStaff::where("ISN", $userEmail)->first();
                }
                if (!$user) {
                    $userId = StaffEmail::where('email', $userEmail)->first();
                    if ($userId) {
                        $user = BotanStaff::where("id", $userId->userId)->first();
                    } else {
                        $res = "Email не найден";
                    }
                }
                if (!$user) {
                    $res = "Пользователь не найден";
                }
            } else {
                $res = 'Вы не авторизованы';
            }
            $sub = substr($chatId, 0, 1);
            if (!$user && $sub != '-') {
                $reply_markup = Keyboard::make([
                    'keyboard' => [['/auth']],
                    'resize_keyboard' => true,
                    'one_time_keyboard' => true
                ]);
                Telegram::sendMessage([
                    'chat_id' => $chatId,
                    'text' => $res,
                    'reply_markup' => $reply_markup,
                ]);
            } else {
                return array('res' => $res, 'user' => $user ? $user : null);
            }
        } catch (\Exception $e) {
            Log::debug('checkUser@index ' . $e->getMessage());
        }
    }
}
