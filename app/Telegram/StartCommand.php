<?php
namespace App\Telegram;

use App\Models\BotanDialogHistories;
use App\Models\StaffTelegramUsers;
use App\Models\TelegramRequestLog;
use App\Repository\Telegram\TelegramInterface;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Keyboard\Keyboard;

class StartCommand extends Command
{

    protected $name = "start";
    protected $description = "Start Command to get you started";
    private $telegramRepository;
    public function __construct(TelegramInterface $telegramRepository)
    {
        $this->telegramRepository = $telegramRepository;
    }

    public function handle()
    {
        try {
            $params = $this->telegramRepository->initParams($this);

            $historyArr = [
                "chatId" => $params['chatId'],
                "telegramUsername" => $params['username'],
                "telegramFirstName" => $params['firstname'],
                "telegramLastName" => $params['lastname'],
                "request" => $params['messageText'],
            ];

            // запись запроса в историю диалогов
            BotanDialogHistories::create($historyArr);

            $this->executeCommand($params['chatId'], $params['username'], $params['firstname'], $params['lastname']);
        } catch (\Exception $e) {
            Log::useDailyFiles(storage_path().'/logs/name-of-log.log');
            Log::info('/start handle ' . $e->getMessage());
        }
    }

    public function executeCommand($chatId, $username, $firstname, $lastname) {

        try {
            $this->replyWithChatAction(['action' => Actions::TYPING]);
            TelegramRequestLog::where('telegramId', $chatId)->delete();
            $telegramUser = StaffTelegramUsers::where('telegramId', $chatId)->first();
            $text = "Привет! Я корпоративный бот группы Компаний Сентрас.";

            if (!$telegramUser) {
                $text .=  "Для доступа нужно будет авторизоваться, нажмите на /auth";
                $reply_markup = Keyboard::make([
                    'keyboard' => [['/auth', '/start']],
                    'resize_keyboard' => true,
                    'one_time_keyboard' => true
                ]);
            } else {
                $result = CheckUser::index($chatId);
                if ($result['user']) {
                    $text = "Привет, ".$result['user']->FIO."! Я корпоративный бот группы Компаний Сентрас.";
                }
                $text .=  "\nВот, что я умею: \n" .
                    "👨‍💼 Найду коллегу /facebook" .
                    "\n"."🏝 Мой отпуск /vacation".
                    "\n"."🏦 О холдинге /holding".
                    "\n"."💡 Заявить о проблеме /problem".
                    "\n"."👩‍⚕️ Запись на прием к врачу  /doctor"
                ;
                // "\nМетодология /methodology".
                // "\n📚 Библиотека /library";
                if ($result['user']->WORKPLACE === 'АО "СК "Сентрас Иншуранс"' || $result['user']->WORKPLACE === '"АО СК Коммеск-Омир"'||$result['user']->WORKPLACE === "АО \"КСЖ Сентрас Коммеск Life\""){
                    $text .= "\n⌚️ Бронирование /booking";
                }
                $keyboard = DefaultKeyboard::getMenuKeyboard($result['user']->WORKPLACE);
                $reply_markup = Keyboard::make([
                    'keyboard' => $keyboard,
                    'resize_keyboard' => true,
                    'one_time_keyboard' => true
                ]);
            }
            $this->replyWithMessage(['text' => $text, 'reply_markup' => $reply_markup]);
            $historyArr = [
                "chatId" => $chatId,
                "telegramUsername" => $username,
                "telegramFirstName" => $firstname,
                "telegramLastName" => $lastname,
                "response" => $text,
            ];
            BotanDialogHistories::create($historyArr);
        }  catch (\Exception $e) {
            Log::debug('/start execute ' . $e->getMessage());
        }
    }
}
